import torch
import torch.nn as nn
from torch.nn.parameter import Parameter
import torch.nn as nn
import torch.nn.functional as F
import math
import numpy as np

def initialize_weight(weight,gaussian):
    n1,n2 = weight.size()[:2]
    receptive_field_size= np.prod(weight.size()[2:])
    stdv =  np.sqrt(2.0 / ((n1 + n2) * receptive_field_size))
    if gaussian:
        weight.normal_(-stdv ,stdv )
    else:
        weight.uniform_(-stdv * np.sqrt(3.0),stdv * np.sqrt(3.0))        
    

def make_weight(template_weight,sign_aligned,gaussian = False):
    weight = torch.zeros_like(template_weight)
    initialize_weight(weight,gaussian)
    if sign_aligned:
        weight.mul_(torch.sign(weight) * torch.sign(template_weight))
    return weight

def print_hook(base_print_foo,additional_text):
    def extended_print():
        return base_print_foo() + ')' +  additional_text
    return extended_print

def fix_layer_params(layer):
    for p in layer.parameters():
        p.requires_grad_(False)
    setattr(layer,'extra_repr',print_hook(layer.extra_repr,'-fixed'))

class fb_alignment_hook(object):
    def __init__(self,layer,sign_aligned = False):
        if isinstance(layer,nn.Linear):
           layer.register_buffer('random_fb_weight',make_weight(layer.weight,sign_aligned))
           layer.register_backward_hook(self.fb_hook_linear)
        elif isinstance(layer,nn.Conv2d):
           assert layer.stride[0] == layer.stride[1] == 1 and \
                  layer.dilation[0] == layer.dilation[1] == 1 and \
                  layer.groups == 1 and \
                  layer.kernel_size[0] == layer.kernel_size[1] and \
                  layer.padding[0] == layer.padding[1] == layer.kernel_size[0] // 2,\
                  'only support feedback alignment for stride=groups=dilation =1 and padding=kernel_size// 2'
           layer.register_buffer('random_fb_weight',make_weight(layer.weight,sign_aligned).permute(1,0,2,3))
           layer.register_backward_hook(self.fb_hook_conv)
        else:
           raise RuntimeError('unsupported layer {} for feedback alignment'.format(layer))        
        setattr(layer,'extra_repr',print_hook(layer.extra_repr,'-feedback aligned'))
        
    def fb_hook_conv(self,conv_layer,grad_in,grad_out):
        
        return (F.conv2d(grad_out[0],conv_layer.random_fb_weight,
                         padding = conv_layer.padding) if grad_in[0] is not None else None,grad_in[1],
                grad_in[2])


    def fb_hook_linear(self,linear_layer,grad_in,grad_out):

        if linear_layer.bias is None:
           return (torch.mm(grad_out[0],linear_layer.random_fb_weight) if grad_in[0] is not None else None,grad_in[1])                
        else:
           return (grad_in[0],torch.mm(grad_out[0],linear_layer.random_fb_weight) if grad_in[1] is not None else None,grad_in[2])


class local_learning_layer(nn.Module):
    def __init__(self,in_neurons,n_classes,return_classifier_out = True,trainable = False,sign_aligned = False,binary_classifier_weights = False,gaussian = False):
        super(local_learning_layer, self).__init__()
        
        self.in_neurons = in_neurons
        self.return_classifier_out = return_classifier_out
        if trainable and sign_aligned:
           warnings.warn('sign_aligned flag is ignored if trainable flag is on')
           

        self.local_classifier = nn.Linear(in_neurons,n_classes,bias = False)
        initialize_weight(self.local_classifier.weight.data,gaussian)
        if binary_classifier_weights:
            self.local_classifier.weight.data.bernoulli_(0.5).mul_(2).sub_(1)
            
        
        if not(trainable):
           if sign_aligned:
              self.register_buffer('backward_weight',make_weight(self.local_classifier.weight,True))
           

        self.trainable = trainable
        self.sign_aligned  = sign_aligned
        
        #if not(self.trainable):
        self.local_classifier.register_backward_hook(self.local_learning_hook)
        
        self.classifier_out = None

    def local_learning_hook(self,local_classifier,grad_in,grad_out):
        if self.trainable:
            return (grad_in[0],grad_in[1])                
        else:
           if self.sign_aligned:
              return (torch.mm(grad_out[0],self.backward_weight),torch.zeros_like(grad_in[1]))
           else:
              return (grad_in[0],torch.zeros_like(grad_in[1]))  
                
    def forward(self,x):
        if self.return_classifier_out:
            return x.detach(),self.local_classifier(x.view(-1,self.in_neurons))
        else:
            self.classifier_out = self.local_classifier(x.view(-1,self.in_neurons))
            return x.detach()
